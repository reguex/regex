import Foundation

extension String {
    public func regex(options: NSRegularExpression.Options = []) -> RegEx? {
        return RegEx(pattern: self, options: options)
    }

    public func findAll(
            regex: RegEx,
            options: RegEx.MatchingOptions = [],
            range: Range<Index>? = nil
    ) -> [Group] {
        return regex.findAll(in: self, options: options, range: range)
    }
}