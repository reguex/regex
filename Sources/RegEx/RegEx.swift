import Foundation

public class RegEx {
    public typealias MatchingOptions = NSRegularExpression.MatchingOptions
    public typealias Options = NSRegularExpression.Options

    private let regularExpression: NSRegularExpression

    public init?(pattern: String, options: Options = []) {
        guard let regularExpression = try? NSRegularExpression(pattern: pattern, options: options) else {
            return nil
        }

        self.regularExpression = regularExpression
    }

    public var pattern: String {
        return regularExpression.pattern
    }

    public var options: Options {
        return regularExpression.options
    }

    public func findAll(in string: String, options: MatchingOptions = [], range: Range<String.Index>? = nil)
                    -> [Group] {

        let range = range ?? string.startIndex..<string.endIndex
        assert(range.lowerBound >= string.startIndex && range.upperBound < string.endIndex, "invalid range")

        let matches = regularExpression.matches(in: string, options: options, range: NSRange(range, in: string))
        return matches.map {
            let matchedRange = Range($0.range, in: string)!
            let matchedString = String(string[matchedRange])

            return Group(text: matchedString, range: matchedRange)
        }
    }
}


public struct Group: CustomStringConvertible {
    public let text: String
    public let range: Range<String.Index>

    public let description: String

    fileprivate init(text: String, range: Range<String.Index>) {
        self.text = text
        self.range = range

        self.description = "Match(\(text), range: \(range))"
    }
}