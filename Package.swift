// swift-tools-version:4.0

import PackageDescription

let package = Package(
        name: "RegEx",
        products: [
            .library(name: "RegEx", targets: ["RegEx"])
        ],
        dependencies: [],
        targets: [
            .target(name: "RegEx")
        ]
)
